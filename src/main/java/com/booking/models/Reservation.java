package com.booking.models;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;
    //   workStage (In Process, Finish, Canceled)

    public Reservation(String reservationId, Customer customer, Employee employee, List<Service> services,
            String workstage) {
        this.reservationId = reservationId;
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.reservationPrice = calculateReservationPrice(customer, services);
        this.workstage = workstage;
    };

    private double calculateReservationPrice(Customer customer, List<Service> serviceList){
        double totalPrice = 0;

        for(Service service : serviceList){
            double price = service.getPrice();
            Membership membership = customer.getMember();

            switch (membership.getMembershipName().toUpperCase()){
                case "NONE":
                    totalPrice = price;
                    break;
                case "SILVER":
                    totalPrice = price * (1 - 0.05);
                    break;
                case "GOLD":
                    totalPrice = price * (1 - 0.1);
                    break;
            }
        }
        return totalPrice;
    }
}
