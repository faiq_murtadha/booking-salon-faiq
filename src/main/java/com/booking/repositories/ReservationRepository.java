package com.booking.repositories;

import java.util.ArrayList;
import java.util.List;

import com.booking.models.Reservation;

public class ReservationRepository {
    private List<Reservation> allReservations;
    private static int reservationCounter = 0;

    public ReservationRepository(){
        this.allReservations = new ArrayList<>();
    }

    public void addReservation(Reservation reservation){
        reservation.setReservationId(generateUniqueReservationID());
        this.allReservations.add(reservation);
    }

    public void cancelReservation(Reservation reservation){
        this.allReservations.remove(reservation);
    }

    public Reservation findReservation(String reservationID){
        for (Reservation reservation : allReservations){
            if (reservation.getReservationId().equals(reservationID)){
                return reservation;
            }
        }
        return null;
    }

    public List<Reservation> getAllReservations(){
        return new ArrayList<>(this.allReservations);
    }

    private String generateUniqueReservationID(){
        reservationCounter++;
        return "Rsv-" + String.format("%02d", reservationCounter);
    }
}
