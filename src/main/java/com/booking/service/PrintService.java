package com.booking.service;

import java.util.List;

import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.ServiceRepository;
import com.booking.repositories.ReservationRepository;
import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Membership;
import com.booking.models.Person;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public static void printServices(List<Service> serviceList){
        int num = 1;
        System.out.println("== Data Service ==");
        System.out.println("+====================================================================+");
        System.out.printf("| %-4s | %-8s | %-25s | %-20s |\n",
                "No.", "ID", "Nama", "Harga");
        System.out.println("+====================================================================+");
        for (Service service : serviceList) {
            System.out.printf("| %-4s | %-8s | %-25s | %-20s |\n",
                num, service.getServiceId(), service.getServiceName(), service.getPrice());
                num++;
        }
        System.out.println("+====================================================================+");
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showRecentReservation(List<Reservation> reservationList){
        List<Reservation> reservations = ReservationService.getAllReservations();
        int num = 1;
        System.out.println("== Data Recent Reservation ==");
        System.out.println("+====================================================================================================+");
        System.out.printf("| %-4s | %-8s | %-11s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Workstage");
        System.out.println("+====================================================================================================+");
        for (Reservation reservation : reservations) {
            if (reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-8s | %-11s | %-15s | %-15s | %-10s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), toStringService(reservation.getServices()), reservation.getReservationPrice(), reservation.getWorkstage());
                num++;
            }
        }
        System.out.println("+====================================================================================================+");
    }

    public static String toStringService(List<Service> services){
        StringBuilder serviceList = new StringBuilder();
        for (Service service : services){
            serviceList.append(service.getServiceName()).append(", ");
        }
        return serviceList.substring(0, serviceList.length() - 2);
    }

    public static void showAllCustomer(List<Person> personList){
        System.out.println("== Data Customer ==");
        System.out.println("+=========================================================================================+");
        System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-15s | %-15s | \n",
                        "No", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out.println("+=========================================================================================+");

        int num = 1;

        for (Person cust : personList){
            if (cust instanceof Customer){
                Customer customer = (Customer) cust;
                Membership member = customer.getMember();

                System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-15s | %-15s | \n",
                                num, cust.getId(), cust.getName(), cust.getAddress(), member.getMembershipName(), customer.getWallet());
                num++;
            }
        }
        System.out.println("+=========================================================================================+");
    }

    public static void showAllEmployee(List<Person> personList){
        System.out.println("== Data Employee ==");
        System.out.println("=========================================================================+");
        System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-15s | \n", 
                                "No", "ID", "Nama", "Alamat", "Pengalaman");
        System.out.println("=========================================================================+");

        int num = 1;

        for (Person emplo : personList){
            if (emplo instanceof Employee){
                Employee employee = (Employee) emplo;

                System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-15s | \n", 
                                num, emplo.getId(), emplo.getName(), emplo.getAddress(), employee.getExperience());
                
                num++;
            }
        }
        System.out.println("=========================================================================+");
    }

    public static void showHistoryReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.println("== Data History Reservation ==");
        System.out.println("+================================================================================================+");
        System.out.printf("| %-4s | %-8s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+================================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Finish") || reservation.getWorkstage().equalsIgnoreCase("Canceled")) {
                System.out.printf("| %-4s | %-8s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), reservation.getServices(), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
            }
        }
        System.out.println("+================================================================================================+");
    }
}
