package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Person;
import com.booking.models.Service;
import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Reservation;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;

public class ReservationService {
    private static List<Reservation> reservations = new ArrayList<>();
    private static Scanner input = new Scanner(System.in);
    private static int reservationCounter = 0;
    
    public static void createReservation(List<Person> personList, List<Service> serviceList){
        boolean doneInputReservation = false;
        PrintService.showAllCustomer(personList);
        System.out.print("Silahkan Masukkan Customer Id: ");
        String custId = input.nextLine();
        Customer selectedCustomer = getCustomerByCustomerId(custId, personList);

        PrintService.showAllEmployee(personList);
        System.out.print("Silahkan Masukkan Employee Id: ");
        String emploId = input.nextLine();
        Employee selectedEmployee = getEmployeeByEmployeeId(emploId, personList);

        List<Service> selectedServices = new ArrayList<>();
        do {
            PrintService.printServices(serviceList);
            System.out.print("Silahkan Masukkan Service Id: ");
            String servId = input.nextLine();
            Service selectedService = getServiceByServiceId(servId, serviceList);
            selectedServices.add(selectedService);
    
            System.out.println("Ingin Pilih service yang lain (Y/T)?");
            String servOption = input.nextLine();
            if(servOption.equals("T") || servOption.equals("t") ){
                doneInputReservation = true;
            }
        } while (!doneInputReservation);
        String reservationId = generateUniqueReservationID();
        Reservation reservation = Reservation.builder()
                                    .reservationId(reservationId)
                                    .customer(selectedCustomer)
                                    .employee(selectedEmployee)
                                    .services(selectedServices)
                                    .workstage("In Process")
                                    .build();
        reservations.add(reservation);
        System.out.println("Booking Berhasil");
        System.out.println("Total Biaya Booking: Rp." + reservation.getReservationPrice());
    }

    public static Customer getCustomerByCustomerId(String custId, List<Person> personList){
        for (Person person : personList){
            if (person instanceof Customer && person.getId().equalsIgnoreCase(custId)){
                return (Customer) person;
            }
        }
        return null;
    }

    public static Employee getEmployeeByEmployeeId(String emploId, List<Person> personList){
        for (Person person : personList){
            if (person instanceof Employee && person.getId().equalsIgnoreCase(emploId)){
                return (Employee) person;
            }
        }
        return null;
    }

    public static Service getServiceByServiceId(String servId, List<Service> serviceList){
        for (Service service : serviceList){
            if (service.getServiceId().equalsIgnoreCase(servId)){
                return service;
            }
        }
        return null;
    }

    public static List<Reservation> getAllReservations(){
        return new ArrayList<>(reservations);
    }
    public static void editReservationWorkstage(){
        
    }

    private static String generateUniqueReservationID(){
        reservationCounter++;
        return "Rsv-" + String.format("%02d", reservationCounter);
    }
    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
